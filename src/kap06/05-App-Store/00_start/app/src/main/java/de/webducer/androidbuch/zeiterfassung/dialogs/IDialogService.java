package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.support.annotation.NonNull;

import java.util.Calendar;

public interface IDialogService {
    /**
     * Ändern des Datums
     *
     * @param originDate Original Calendar Objekt
     * @param filedId    ID des Feldes in ViewModel für die Benachrichtigung
     */
    void changeDate(@NonNull Calendar originDate, int fieldId);

    /**
     * Ändern der Uhrzeit
     *
     * @param originTime Original Calendar Objekt
     * @param fieldId    ID des Feldes in ViewModel für die Benachrichtigung
     */
    void changeTime(@NonNull Calendar originTime, int fieldId);

    /**
     * Setzen der neuen Daten und Benachrichtigung
     *
     * @param newDateTime neues Calendar Wert
     * @param fieldID     ID des Feldes in ViewModel
     */
    void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldId);
}
