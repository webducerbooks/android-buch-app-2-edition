package de.webducer.androidbuch.zeiterfassung;

import android.databinding.Observable;

import org.junit.Test;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.viewmodels.EditDataViewModel;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class EditDataViewModelTests {
    // Pause
    @Test
    public void setPause_WithDifferentDuration_RaisePropertyChangeForPause() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.setPause(10);
        sut.addOnPropertyChangedCallback(listener);
        int newPause = 20;

        // Act
        sut.setPause(newPause);

        // Assert
        verify(listener, times(1)).onPropertyChanged(any(Observable.class), eq(de.webducer.androidbuch.zeiterfassung.BR.pause));
        assertThat(sut.getPause(), is(newPause));
    }

    @Test
    public void setPause_WithSameDuration_RaiseNoPropertyChange() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.setPause(10);
        sut.addOnPropertyChangedCallback(listener);
        int newPause = 10;

        // Act
        sut.setPause(newPause);

        // Assert
        verify(listener, times(0)).onPropertyChanged(any(Observable.class), eq(de.webducer.androidbuch.zeiterfassung.BR.pause));
        assertThat(sut.getPause(), is(newPause));
    }
    // Kommentar
    @Test
    public void setComment_WithDifferentValues_RaisePropertyChangeForComment() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.setComment("Hallo Welt!");
        sut.addOnPropertyChangedCallback(listener);
        final String newComment = "Goodbye World!";

        // Act
        sut.setComment(newComment);

        // Assert
        verify(listener, times(1)).onPropertyChanged(any(Observable.class), eq(BR.comment));
        assertThat(sut.getComment(), is(newComment));
    }

    @Test
    public void setComment_WithSameValue_RaiseNoPropertyChange() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        sut.setComment("Hallo Welt!");
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.addOnPropertyChangedCallback(listener);
        String newComment = "Hallo Welt!";

        // Act
        sut.setComment(newComment);

        // Assert
        verify(listener, times(0)).onPropertyChanged(any(Observable.class), anyInt());
        assertThat(sut.getComment(), is(newComment));
    }

    // Startzeit
    @Test
    public void setStartTime_WithDifferentValues_RaisePropertyChangeForStartTime() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        Calendar oldValue = Calendar.getInstance();
        oldValue.set(2017, 00, 28, 23, 47, 55);
        sut.setStartTime(oldValue);
        sut.addOnPropertyChangedCallback(listener);
        Calendar newValue = Calendar.getInstance();
        newValue.set(2017, 00, 29, 10, 11, 12);

        // Act
        sut.setStartTime(newValue);

        // Assert
        verify(listener, times(1)).onPropertyChanged(any(Observable.class), eq(BR.startTime));
        assertThat(sut.getStartTime(), is(newValue));
    }

    @Test
    public void setStartTime_WithSameValue_RaiseNoPropertyChange() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Calendar oldValue = Calendar.getInstance();
        oldValue.set(2017, 00, 28, 23, 47, 55);
        sut.setStartTime(oldValue);
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.addOnPropertyChangedCallback(listener);
        Calendar newValue = Calendar.getInstance();
        newValue.set(2017, 00, 28, 23, 47, 55);

        // Act
        sut.setStartTime(newValue);

        // Assert
        verify(listener, times(0)).onPropertyChanged(any(Observable.class), anyInt());
        assertThat(sut.getStartTime(), is(oldValue));
    }

    // Endzeit
    @Test
    public void setEndTime_WithDifferentValues_RaisePropertyChangeForStartTime() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        Calendar oldValue = Calendar.getInstance();
        oldValue.set(2017, 00, 28, 23, 47, 55);
        sut.setEndTime(oldValue);
        sut.addOnPropertyChangedCallback(listener);
        Calendar newValue = Calendar.getInstance();
        newValue.set(2017, 00, 29, 10, 11, 12);

        // Act
        sut.setEndTime(newValue);

        // Assert
        verify(listener, times(1)).onPropertyChanged(any(Observable.class), eq(BR.endTime));
        assertThat(sut.getEndTime(), is(newValue));
    }

    @Test
    public void setEndTime_WithSameValue_RaiseNoPropertyChange() {
        // Arrange
        EditDataViewModel sut = new EditDataViewModel();
        Calendar oldValue = Calendar.getInstance();
        oldValue.set(2017, 00, 28, 23, 47, 55);
        sut.setEndTime(oldValue);
        Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
        sut.addOnPropertyChangedCallback(listener);
        Calendar newValue = Calendar.getInstance();
        newValue.set(2017, 00, 28, 23, 47, 55);

        // Act
        sut.setEndTime(newValue);

        // Assert
        verify(listener, times(0)).onPropertyChanged(any(Observable.class), anyInt());
        assertThat(sut.getEndTime(), is(oldValue));
    }

}
