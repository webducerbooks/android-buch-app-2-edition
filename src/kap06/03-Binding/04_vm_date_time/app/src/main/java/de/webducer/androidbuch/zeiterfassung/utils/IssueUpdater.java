package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.MalformedURLException;

import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class IssueUpdater extends AsyncTask<Void, Void, Void> {

    private final Context _context;

    public IssueUpdater(Context context) {
        _context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        // Client initialisieren
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(IssueHelper.FILTERED_ISSUE_URI)
                    .build();

            Response response = client.newCall(request).execute();

            if (!response.isSuccessful()) {
                // Fehler bei der Abfrage der Daten
                return null;
            }

            // Zurückgeben der Daten als String
            BitbucketIssue[] issues = IssueHelper.parseIssues(response.body().string());

            // In Datenbak speichern
            IssueHelper.saveToDatabase(_context, issues);
        } catch (MalformedURLException e) {
            // URL konnte nicht interpretiert werden
            e.printStackTrace();
        } catch (IOException e) {
            // Fehler beim Zugriff auf das Internet
            e.printStackTrace();
        }

        return null;
    }
}
