package de.webducer.androidbuch.zeiterfassung.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.BR;

public class EditDataViewModel extends BaseObservable {
    private String _comment;
    private int _pause;
    private Calendar _startTime;
    private Calendar _endTime;

    @Bindable
    public String getComment() {
        return _comment;
    }

    public void setComment(String comment) {
        if (_comment == null && comment == null) {
            // Keine Änderung, beide Werte NULL
            return;
        }

        if (_comment != null && _comment.equals(comment)) {
            // Keine Änderung, beide Werte gleich
            return;
        }

        // Änderungen übernehmen und beachrichtigen
        this._comment = comment;
        notifyPropertyChanged(BR.comment);
    }

    @Bindable
    public int getPause() {
        return _pause;
    }

    public void setPause(int pause) {
        if (_pause == pause) {
            return;
        }

        _pause = pause;
        notifyPropertyChanged(BR.pause);
    }

    @Bindable
    public Calendar getStartTime() {
        return _startTime;
    }

    public void setStartTime(Calendar startTime) {
        if (_startTime == null && startTime == null) {
            // Beide Werte NULL
            return;
        }

        if (_startTime != null && _startTime.equals(startTime)) {
            // Beide Werte sind geleich
        }

        _startTime = startTime;
        notifyPropertyChanged(BR.startTime);
    }

    @Bindable
    public Calendar getEndTime() {
        return _endTime;
    }

    public void setEndTime(Calendar endTime) {
        if (_endTime == null && endTime == null) {
            // Beide Werte NULL
            return;
        }

        if (_endTime != null && _startTime.equals(endTime)) {
            // Beide Werte sind geleich
        }

        _endTime = endTime;
        notifyPropertyChanged(BR.endTime);
    }
}
