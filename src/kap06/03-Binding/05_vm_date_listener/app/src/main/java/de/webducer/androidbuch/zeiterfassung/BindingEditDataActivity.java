package de.webducer.androidbuch.zeiterfassung;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityBindingEditDataBinding;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateServiceDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.IDialogService;
import de.webducer.androidbuch.zeiterfassung.viewmodels.EditDataViewModel;

public class BindingEditDataActivity extends AppCompatActivity implements IDialogService {
    public static final String ID_KEY = "TimeDataId";

    // View Model
    private EditDataViewModel _viewModel = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialisierung von Binding
        ActivityBindingEditDataBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_binding_edit_data);

        // Binden des ViewModels
        _viewModel = new EditDataViewModel();
        binding.setEditData(_viewModel);
    }

    @Override
    protected void onStart() {
        super.onStart();

        _viewModel.setDialogService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        _viewModel.setDialogService(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        _viewModel.setComment("Comment over Binding and View Model");
        _viewModel.setPause(30);
        _viewModel.setStartTime(Calendar.getInstance());
    }

    @Override
    public void changeDate(@NonNull Calendar originDate, int filedId) {
        // Dialog starten
        Calendar date = originDate == null ? Calendar.getInstance() : originDate;
        ChangeDateServiceDialog.showDialog(getSupportFragmentManager(), date, filedId);
    }

    @Override
    public void changeTime(@NonNull Calendar originTime, int fieldId) {
        // Dialog starten
    }

    @Override
    public void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldID) {
        _viewModel.notifyDateChanged(newDateTime, fieldID);
    }
}
