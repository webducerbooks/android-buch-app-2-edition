package de.webducer.androidbuch.zeiterfassung;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.io.IOException;

import de.webducer.androidbuch.zeiterfassung.adapter.IssueAdapter;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.utils.IssueHelper;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class InfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int _LOADER_ID = 200;
    private RecyclerView _issueList;
    private IssueAdapter _adapter;
    // Update Thread von OK HTTP
    private Call _updateCall = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // Initialisierung der UI
        _issueList = findViewById(R.id.IssueList);
        _issueList.setLayoutManager(new LinearLayoutManager(this));
        _adapter = new IssueAdapter(this, null);
        _issueList.setHasFixedSize(true);
        _issueList.setAdapter(_adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Aktualisieren der Daten mit async OK HTTP
        updateData();
        getSupportLoaderManager().restartLoader(_LOADER_ID, null, this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        getSupportLoaderManager().destroyLoader(_LOADER_ID);

        // Update der daten aus dem Internet abbrechen, falls es nocht nicht abgeschlossen wurde
        if (_updateCall != null) {
            _updateCall.cancel();
        }
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, @Nullable Bundle bundle) {
        CursorLoader loader = null;

        switch (loaderId) {
            case _LOADER_ID:
                loader = new CursorLoader(this,
                        TimeDataContract.IssueData.CONTENT_URI,
                        null,
                        null,
                        null,
                        TimeDataContract.IssueData.Columns.NUMBER + " DESC");
                break;
        }

        return loader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case _LOADER_ID:
                _adapter.swapData(data);
                break;
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        switch (loader.getId()) {
            case _LOADER_ID:
                _adapter.swapData(null);
                break;
        }
    }

    private void updateData() {
        // Client initialisieren
        OkHttpClient client = new OkHttpClient();

        // Request erstellen
        Request request = new Request.Builder()
                .url(IssueHelper.FILTERED_ISSUE_URI)
                .build();

        // Asynchronen Aufruf erstellen
        _updateCall = client.newCall(request);
        _updateCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // Behandlung im Fehlerfall
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                // Behandlung im positiven Fall
                if (!response.isSuccessful()) {
                    // Fehler bei der Abfrage der Daten
                    return;
                }

                // Zurückgeben der Daten
                // Parsen mit GSon
                BitbucketIssue[] issues = IssueHelper.parseIssues(response.body().string());

                // Speichern in die Datenbank
                IssueHelper.saveToDatabase(getApplicationContext(), issues);
            }
        });
    }
}
