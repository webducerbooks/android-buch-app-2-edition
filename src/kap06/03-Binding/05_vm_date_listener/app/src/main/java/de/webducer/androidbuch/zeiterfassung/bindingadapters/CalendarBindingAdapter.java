package de.webducer.androidbuch.zeiterfassung.bindingadapters;

import android.databinding.BindingAdapter;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

public class CalendarBindingAdapter {
    @BindingAdapter("time")
    public static void setTime(EditText view, Calendar time) {
        String timeString = "";
        if (time != null) {
            // Formatierung initialisieren
            DateFormat formatter = android.text.format.DateFormat.getTimeFormat(view.getContext());
            timeString = formatter.format(time.getTime());
        }

        if (timeString.equals(view.getText().toString())) {
            // Keine Aktualisierung, noch derselbe Wert
            return;
        }

        view.setText(timeString);
    }

    @BindingAdapter("date")
    public static void setDate(EditText view, Calendar date) {
        String dateString = "";
        if (date != null) {
            // Formatierung initialisieren
            DateFormat formatter = android.text.format.DateFormat.getDateFormat(view.getContext());
            dateString = formatter.format(date.getTime());
        }

        if (dateString.equals(view.getText().toString())) {
            // Keine Aktualisierung, noch derselbe Wert
            return;
        }

        view.setText(dateString);
    }
}
