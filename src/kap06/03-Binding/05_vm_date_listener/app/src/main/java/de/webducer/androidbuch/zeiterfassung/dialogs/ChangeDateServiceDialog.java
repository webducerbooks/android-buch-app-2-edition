package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.DatePicker;

import java.util.Calendar;

public class ChangeDateServiceDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    // Konstanten
    private static final String _DATE_KEY = "Key_Date";
    private static final String _FIELD_KEY = "Key_FieldId";
    private Calendar _date = Calendar.getInstance();
    private int _fieldId;

    // Factory
    public static void showDialog(@NonNull FragmentManager manager, @NonNull Calendar date, int fieldId) {
        // Parameter als Bundle zusammenfassen
        Bundle extras = new Bundle();
        extras.putLong(_DATE_KEY, date.getTimeInMillis());
        extras.putInt(_FIELD_KEY, fieldId);

        // Dialog starten
        ChangeDateServiceDialog dialog = new ChangeDateServiceDialog();
        dialog.setArguments(extras);
        dialog.show(manager, "DateDialog");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Interface für Änderungen bestimmen
        if (getActivity() instanceof IDialogService == false) {
            // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
            throw new UnsupportedOperationException("Please implement IDialogService in your calling activity");
        }

        // Datum als Parameter auslesen
        if (getArguments() == null || !getArguments().containsKey(_DATE_KEY)) {
            throw new NullPointerException("Date is not set");
        }

        // Feld ID als Parameter auslesen
        if (!getArguments().containsKey(_FIELD_KEY)) {
            throw new NullPointerException("Field ID is not set");
        }

        _date.setTimeInMillis(getArguments().getLong(_DATE_KEY));
        _fieldId = getArguments().getInt(_FIELD_KEY);

        // Initialisieren des Dialoges
        DatePickerDialog dialog = new DatePickerDialog(getContext(),
                this, // Änderungscallback
                _date.get(Calendar.YEAR), // Jahr
                _date.get(Calendar.MONTH), // Monat
                _date.get(Calendar.DAY_OF_MONTH)); // Tag

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Interface für Änderungen bestimmen
        if (getActivity() instanceof IDialogService == false) {
            // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
            throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
        }

        // Interface initialisieren
        IDialogService listener = (IDialogService) getActivity();

        // Datum ändern
        _date.set(year, month, dayOfMonth);

        // Änderung kommunizieren
        listener.notifyDateTimeChanged(_date, _fieldId);
    }
}
