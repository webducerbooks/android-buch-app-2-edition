package de.webducer.androidbuch.zeiterfassung.bindingadapters;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.widget.EditText;

public class IntegerBindingAdapter {
    @BindingAdapter("android:text")
    public static void setText(EditText view, int value) {
        // "0" als "" darstellen
        String stringValue = value == 0 ? "" : Integer.toString(value);

        // Prüfen, ob der Wert bereits im View steht
        if (stringValue.equals(view.getText().toString())) {
            return;
        }

        // Neuen Wert setzen
        view.setText(stringValue);
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static int getText(EditText view) {
        if (view.getText() == null || view.getText().toString().isEmpty()) {
            // Wenn kein Wert eingegeben ist, 0
            return 0;
        }

        // Konvertierung
        String stringValue = view.getText().toString();
        int intValue = 0;

        try {
            intValue = Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            // Text ist keine Nummer
            e.printStackTrace();
        }

        return intValue;
    }
}
