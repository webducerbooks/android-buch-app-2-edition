package de.webducer.androidbuch.zeiterfassung.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.TimePicker;

import java.util.Calendar;

public class ChangeTimeServiceDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    // Konstanten
    private static final String _TIME_KEY = "Key_Time";
    private static final String _FIELD_KEY = "Key_FieldId";
    private Calendar _time = Calendar.getInstance();
    private int _fieldId;

    // Factory
    public static void showDialog(@NonNull FragmentManager manager, @NonNull Calendar date, int fieldId) {
        // Parameter als Bundle zusammenfassen
        Bundle extras = new Bundle();
        extras.putLong(_TIME_KEY, date.getTimeInMillis());
        extras.putInt(_FIELD_KEY, fieldId);

        // Dialog starten
        ChangeTimeServiceDialog dialog = new ChangeTimeServiceDialog();
        dialog.setArguments(extras);
        dialog.show(manager, "TimeDialog");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Interface für Änderungen bestimmen
        if (getActivity() instanceof IDialogService == false) {
            // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
            throw new UnsupportedOperationException("Please implement IDialogService in your calling activity");
        }

        // Datum als Parameter auslesen
        if (getArguments() == null || !getArguments().containsKey(_TIME_KEY)) {
            throw new NullPointerException("Time is not set");
        }

        // Feld ID als Parameter auslesen
        if (!getArguments().containsKey(_FIELD_KEY)) {
            throw new NullPointerException("Field ID is not set");
        }

        _time.setTimeInMillis(getArguments().getLong(_TIME_KEY));
        _fieldId = getArguments().getInt(_FIELD_KEY);

        // Anzeige 12 / 24h
        boolean is24 = android.text.format.DateFormat.is24HourFormat(getContext());

        TimePickerDialog dialog = new TimePickerDialog(getContext(),
                this,
                _time.get(Calendar.HOUR_OF_DAY),
                _time.get(Calendar.MINUTE),
                is24);

        return dialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Interface für Änderungen bestimmen
        if (getActivity() instanceof IDialogService == false) {
            // Interface nicht umgesetzt, keine Nutzung des Dialoges möglich
            throw new UnsupportedOperationException("Please implement IDualogService in your calling activity");
        }

        // Interface initialisieren
        IDialogService listener = (IDialogService) getActivity();

        _time.set(Calendar.HOUR_OF_DAY, hourOfDay);
        _time.set(Calendar.MINUTE, minute);

        listener.notifyDateTimeChanged(_time, _fieldId);
    }
}
