package de.webducer.androidbuch.zeiterfassung.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.BR;
import de.webducer.androidbuch.zeiterfassung.dialogs.IDialogService;

public class EditDataViewModel extends BaseObservable {
    private boolean _firstFocus = true;
    private String _comment;
    private int _pause;
    private Calendar _startTime;
    private Calendar _endTime;
    private IDialogService _dialogService;

    public void setDialogService(IDialogService dialogService) {
        _dialogService = dialogService;
    }

    @Bindable
    public String getComment() {
        return _comment;
    }

    public void setComment(String comment) {
        if (_comment == null && comment == null) {
            // Keine Änderung, beide Werte NULL
            return;
        }

        if (_comment != null && _comment.equals(comment)) {
            // Keine Änderung, beide Werte gleich
            return;
        }

        // Änderungen übernehmen und beachrichtigen
        this._comment = comment;
        notifyPropertyChanged(BR.comment);
    }

    @Bindable
    public int getPause() {
        return _pause;
    }

    public void setPause(int pause) {
        if (_pause == pause) {
            return;
        }

        _pause = pause;
        notifyPropertyChanged(BR.pause);
    }

    @Bindable
    public Calendar getStartTime() {
        return _startTime;
    }

    public void setStartTime(Calendar startTime) {
        if (_startTime == null && startTime == null) {
            // Beide Werte NULL
            return;
        }

        if (_startTime != null && _startTime.equals(startTime)) {
            // Beide Werte sind geleich
        }

        _startTime = startTime;
        notifyPropertyChanged(BR.startTime);
    }

    @Bindable
    public Calendar getEndTime() {
        return _endTime;
    }

    public void setEndTime(Calendar endTime) {
        if (_endTime == null && endTime == null) {
            // Beide Werte NULL
            return;
        }

        if (_endTime != null && _startTime.equals(endTime)) {
            // Beide Werte sind geleich
        }

        _endTime = endTime;
        notifyPropertyChanged(BR.endTime);
    }

    public void changeStartDateOnFocus(boolean hasFocus) {
        if (!focusFired(hasFocus)) {
            return;
        }

        changeStartDate();
    }

    public void changeStartDate() {
        if (_dialogService == null) {
            return;
        }

        _dialogService.changeDate(getStartTime(), BR.startTime);
    }

    public void changeEndDateOnFocus(boolean hasFocus) {
        if (!focusFired(hasFocus)) {
            return;
        }

        changeEndDate();
    }

    public void changeEndDate() {
        if (_dialogService == null) {
            return;
        }

        _dialogService.changeDate(getEndTime(), BR.endTime);
    }

    public void changeStartTimeOnFocus(boolean hasFocus) {
        if (!focusFired(hasFocus)) {
            return;
        }

        changeStartTime();
    }

    public void changeStartTime() {
        if (_dialogService == null) {
            return;
        }

        _dialogService.changeTime(getStartTime(), BR.startTime);
    }

    public void changeEndTimeOnFocus(boolean hasFocus) {
        if (!focusFired(hasFocus)) {
            return;
        }

        changeEndTime();
    }

    public void changeEndTime() {
        if (_dialogService == null) {
            return;
        }

        _dialogService.changeTime(getEndTime(), BR.endTime);
    }

    private boolean focusFired(boolean hasFocus) {
        if (_firstFocus) {
            _firstFocus = false;
            return false;
        }

        return hasFocus;
    }

    public void notifyDateChanged(@NonNull Calendar newDate, int fieldId) {
        // Startdatum
        if (fieldId == BR.startTime) {
            setStartTime(newDate);
            return;
        }

        // Enddatum
        if (fieldId == BR.endTime) {
            setEndTime(newDate);
        }
    }
}
