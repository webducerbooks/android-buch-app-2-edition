package de.webducer.androidbuch.zeiterfassung.dialogs;

public interface IConfirmDeleteListener {
    void confirmDelete(long id, int position);
}
