package de.webducer.androidbuch.zeiterfassung.models;

public class BitbucketIssueContent {
    public String raw;
    public String html;
}
