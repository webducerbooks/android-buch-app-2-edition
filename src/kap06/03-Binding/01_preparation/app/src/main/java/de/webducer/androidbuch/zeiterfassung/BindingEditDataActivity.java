package de.webducer.androidbuch.zeiterfassung;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityBindingEditDataBinding;

public class BindingEditDataActivity extends AppCompatActivity {
    public static final String ID_KEY = "TimeDataId";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialisierung von Binding
        ActivityBindingEditDataBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_binding_edit_data);
    }
}
