package de.webducer.androidbuch.zeiterfassung.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import de.webducer.androidbuch.zeiterfassung.BR;

public class EditDataViewModel extends BaseObservable {
    private String _comment;
    private int _pause;

    @Bindable
    public String getComment() {
        return _comment;
    }

    public void setComment(String comment) {
        if (_comment == null && comment == null) {
            // Keine Änderung, beide Werte NULL
            return;
        }

        if (_comment != null && _comment.equals(comment)) {
            // Keine Änderung, beide Werte gleich
            return;
        }

        // Änderungen übernehmen und beachrichtigen
        this._comment = comment;
        notifyPropertyChanged(BR.comment);
    }

    @Bindable
    public int getPause() {
        return _pause;
    }

    public void setPause(int pause) {
        if (_pause == pause) {
            return;
        }

        _pause = pause;
        notifyPropertyChanged(BR.pause);
    }
}
