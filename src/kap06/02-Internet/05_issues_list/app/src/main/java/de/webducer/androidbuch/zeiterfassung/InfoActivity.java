package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import de.webducer.androidbuch.zeiterfassung.adapter.IssueAdapter;
import de.webducer.androidbuch.zeiterfassung.utils.IssueUpdater;

public class InfoActivity extends AppCompatActivity {

    private RecyclerView _issueList;
    private IssueAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // Initialisierung der UI
        _issueList = findViewById(R.id.IssueList);
        _issueList.setLayoutManager(new LinearLayoutManager(this));
        _adapter = new IssueAdapter(this, null);
        _issueList.setHasFixedSize(true);
        _issueList.setAdapter(_adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Daten mit Updater lesen
        new IssueUpdater(_adapter).execute();
    }
}
