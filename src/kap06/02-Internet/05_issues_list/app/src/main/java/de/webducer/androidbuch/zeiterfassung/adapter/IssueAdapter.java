package de.webducer.androidbuch.zeiterfassung.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.webducer.androidbuch.zeiterfassung.R;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.IssueViewHolder> {

    private final Context _context;
    private BitbucketIssue[] _issues;

    public IssueAdapter(@NonNull Context context, BitbucketIssue[] issues) {
        _context = context;
        _issues = issues == null ? new BitbucketIssue[0] : issues;
    }

    @NonNull
    @Override
    public IssueViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(_context);
        View view = inflater.inflate(R.layout.item_issue_data, viewGroup, false);
        return new IssueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueViewHolder issueViewHolder, int i) {
        if (i >= _issues.length) {
            // Index größer, als die Datenmenge
            return;
        }

        BitbucketIssue issue = _issues[i];
        issueViewHolder.Number.setText(String.valueOf(issue.id));
        issueViewHolder.Title.setText(issue.title);
        issueViewHolder.Priority.setText(issue.priority);
        issueViewHolder.State.setText(issue.state);
    }

    @Override
    public int getItemCount() {
        return _issues.length;
    }

    public void swapData(BitbucketIssue[] issues) {
        _issues = issues == null ? new BitbucketIssue[0] : issues;
        notifyDataSetChanged();
    }

    public class IssueViewHolder extends RecyclerView.ViewHolder {
        final TextView Number;
        final TextView Title;
        final TextView Priority;
        final TextView State;

        public IssueViewHolder(@NonNull View itemView) {
            super(itemView);

            // Init Layout
            Number = itemView.findViewById(R.id.NumberValue);
            Title = itemView.findViewById(R.id.TitleValue);
            Priority = itemView.findViewById(R.id.PriorityValue);
            State = itemView.findViewById(R.id.StateValue);
        }
    }
}
