package de.webducer.androidbuch.zeiterfassung.models;

public class BitbucketIssue {
    public String title;
    public int id;
    public String state;
    public String priority;
    public BitbucketIssueContent content;
}
