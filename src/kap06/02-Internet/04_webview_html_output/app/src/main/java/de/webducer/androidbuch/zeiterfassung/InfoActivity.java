package de.webducer.androidbuch.zeiterfassung;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import de.webducer.androidbuch.zeiterfassung.utils.IssueUpdater;

public class InfoActivity extends AppCompatActivity {

    private WebView _webContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        // Initialisierung der UI
        _webContent = findViewById(R.id.WebContent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Daten mit Updater lesen
        new IssueUpdater(_webContent).execute();
    }
}
