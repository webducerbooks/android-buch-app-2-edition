package de.webducer.androidbuch.zeiterfassung;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppNavigationTests {

    @Rule
    public ActivityTestRule<TimeTrackingActivity> mActivityTestRule = new ActivityTestRule<>(TimeTrackingActivity.class);

    @Test
    public void appNavigationTests() {
        ViewInteraction button = onView(
                allOf(withId(R.id.StartCommand),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                4),
                        isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.StartCommand), withText("Starten"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                4),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.EndCommand), withText("Beenden"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                5),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.ListDataMenuItem), withContentDescription("Auflistung"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.DataList),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                2)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction textView = onView(
                allOf(withId(R.id.LabelComment), withText("Kommentar:"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                8),
                        isDisplayed()));
        textView.check(matches(withText("Kommentar:")));

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Nach oben"),
                        childAtPosition(
                                allOf(withId(R.id.action_bar),
                                        childAtPosition(
                                                withId(R.id.action_bar_container),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        pressBack();

        ViewInteraction button2 = onView(
                allOf(withId(R.id.EndCommand),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                5),
                        isDisplayed()));
        button2.check(matches(isDisplayed()));
    }

    @Test
    public void appNavigationTestsOptimized() {
        // Suche nach dem Button
        ViewInteraction startButton = onView(withId(R.id.StartCommand));
        // Prüfen, dass der Button angezeigt wird
        startButton.check(matches(isDisplayed()));

        // Auf den Button klicken
        startButton.perform(click());

        // Beenden-Button anklicken
        ViewInteraction endButton = onView(withId(R.id.EndCommand));
        endButton.perform(click());

        // Auflistung im Menü aufrufen
        ViewInteraction listAction = onView(withId(R.id.ListDataMenuItem));
        listAction.perform(click());

        // Erstes Element im RecyclerView anklicken
        onView(withId(R.id.DataList))
                .perform(
                        RecyclerViewActions.actionOnItemAtPosition(0, click())
                );

        // Prüfen auf den Kommentar-Text
        ViewInteraction comment = onView(withId(R.id.LabelComment));
        comment.check(matches(withText(R.string.LabelComment)));

        // Navigieren eine Ebene nach oben
        ViewInteraction navigate = onView(
                allOf(withContentDescription(R.string.abc_action_bar_up_description),
                        withParent(allOf(withId(R.id.action_bar),
                                withParent(withId(R.id.action_bar_container)))),
                        isDisplayed()));
        navigate.perform(click());

        // Navigation über Hardware-Button
        pressBack();

        // Beenden-Button testen
        endButton = onView(withId(R.id.EndCommand));
        endButton.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
