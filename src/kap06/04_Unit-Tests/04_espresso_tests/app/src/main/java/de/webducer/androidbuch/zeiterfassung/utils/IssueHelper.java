package de.webducer.androidbuch.zeiterfassung.utils;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;

import com.google.gson.Gson;

import java.util.ArrayList;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketIssue;
import de.webducer.androidbuch.zeiterfassung.models.BitbucketResponse;

public class IssueHelper {
    // Basis-Zugriffspunkt
    private static final String _ISSUE_BASE_URI =
            "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues?q=";

    // Filter
    private static final String _ISSUE_FILTER =
            "(state = \"new\" OR state = \"on hold\" OR state = \"open\")"
                    + " AND updated_on > \"2017-02-01T00:00+02:00\""
                    + " AND component != \"395592\"";
    public static final String FILTERED_ISSUE_URI =
            _ISSUE_BASE_URI
                    + Uri.encode(_ISSUE_FILTER);

    private final static BitbucketIssue[] _DEFAULT_RETURN_VALUE = new BitbucketIssue[0];

    public static BitbucketIssue[] parseIssues(String jsonContent) {
        // Prüfen des Inhaltes
        if (jsonContent == null || jsonContent.isEmpty()) {
            // Rückgabe einer leeren Liste
            return _DEFAULT_RETURN_VALUE;
        }

        // Initialisieren der Bibliothek
        Gson g = new Gson();
        // Deserialisieren von JSON
        BitbucketResponse response = g.fromJson(jsonContent, BitbucketResponse.class);

        return response.values;
    }

    public static void saveToDatabase(Context context, BitbucketIssue[] issues) {
        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        ContentProviderOperation.Builder builder = ContentProviderOperation.newDelete(TimeDataContract.IssueData.CONTENT_URI);
        operations.add(builder.build());

        for (BitbucketIssue issue : issues) {
            ContentValues values = new ContentValues();
            values.put(TimeDataContract.IssueData.Columns.NUMBER, issue.id);
            values.put(TimeDataContract.IssueData.Columns.TITLE, issue.title);
            values.put(TimeDataContract.IssueData.Columns.PRIORITY, issue.priority);
            values.put(TimeDataContract.IssueData.Columns.STATE, issue.state);

            builder = ContentProviderOperation.newInsert(TimeDataContract.IssueData.CONTENT_URI)
                    .withValues(values);
            operations.add(builder.build());
        }

        try {
            context.getContentResolver().applyBatch(TimeDataContract.AUTHORITY, operations);
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
