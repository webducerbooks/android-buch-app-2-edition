package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.databinding.ActivityBindingEditDataBinding;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeDateServiceDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.ChangeTimeServiceDialog;
import de.webducer.androidbuch.zeiterfassung.dialogs.IDialogService;
import de.webducer.androidbuch.zeiterfassung.viewmodels.EditDataViewModel;

public class BindingEditDataActivity extends AppCompatActivity implements IDialogService {
    public static final String ID_KEY = "TimeDataId";

    private static final long _NO_DATA = -1L;
    private long _timeDataId = _NO_DATA;

    // Sicherungsfeld
    private boolean _isRestored = false;

    // Konstanten für die Zustandssicherung
    private static final String _START_DATE_TIME_KEY = "Key_StartTime";
    private static final String _END_DATE_TIME_KEY = "Key_EndTime";
    private static final String _PAUSE_KEY = "Key_Pause";
    private static final String _COMMENT_KEY = "Key_Comment";

    // View Model
    private EditDataViewModel _viewModel = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Auslesen der übergebenen Metainformationen, flass welche da sind
        _timeDataId = getIntent().getLongExtra(
                ID_KEY, // Key für die Daten
                _NO_DATA // Standardwert, falls nichts übergeben wurde
        );

        // Initialisierung von Binding
        ActivityBindingEditDataBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_binding_edit_data);

        // Binden des ViewModels
        _viewModel = new EditDataViewModel();
        binding.setEditData(_viewModel);

        // Benutzereingaben verhindern
        binding.StartDateValue.setKeyListener(null);
        binding.StartTimeValue.setKeyListener(null);
        binding.EndDateValue.setKeyListener(null);
        binding.EndTimeValue.setKeyListener(null);

        // Wiederherstellen der Daten
        if (savedInstanceState != null) {
            _isRestored = savedInstanceState.containsKey(_START_DATE_TIME_KEY);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Datumsfelder als Anzahl der Millisekunden merken
        outState.putLong(_START_DATE_TIME_KEY, _viewModel.getStartTime().getTimeInMillis());
        outState.putLong(_END_DATE_TIME_KEY, _viewModel.getEndTime().getTimeInMillis());

        // Pause nach der Konvertierung in Integer merken
        outState.putInt(_PAUSE_KEY, _viewModel.getPause());

        // Kommentar sichern
        outState.putString(_COMMENT_KEY, _viewModel.getComment());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Startdatum
        long milliseconds = savedInstanceState.getLong(_START_DATE_TIME_KEY, 0L);
        if (milliseconds > 0) {
            Calendar startTime = Calendar.getInstance();
            startTime.setTimeInMillis(milliseconds);
            _viewModel.setStartTime(startTime);
        }

        // Enddatum
        milliseconds = savedInstanceState.getLong(_END_DATE_TIME_KEY, 0L);
        if (milliseconds > 0) {
            Calendar endTime = Calendar.getInstance();
            endTime.setTimeInMillis(milliseconds);
            _viewModel.setEndTime(endTime);
        }

        // Pause
        int pause = savedInstanceState.getInt(_PAUSE_KEY, 0);
        _viewModel.setPause(pause);

        // Comment
        _viewModel.setComment(savedInstanceState.getString(_COMMENT_KEY, ""));
    }

    @Override
    protected void onStart() {
        super.onStart();

        _viewModel.setDialogService(this);

        // Laden der Daten, falls diese nicht weiderhergestellt wurden
        if (_isRestored) {
            return;
        }

        // Laden der Daten, falls diese vorhanden sind
        if (_timeDataId == _NO_DATA) {
            return;
        }

        loadData();
    }

    @Override
    protected void onStop() {
        super.onStop();

        _viewModel.setDialogService(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Zurück Button in der Action Bar
            case android.R.id.home:
                saveData();
                // Kein break oder return hier, da das Menü von Android noch verarbeitet werden soll

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Speichern der Daten beim Verlassen der Activity
        saveData();
    }

    @Override
    public void changeDate(@NonNull Calendar originDate, int fieldId) {
        // Dialog starten
        Calendar date = originDate == null ? Calendar.getInstance() : originDate;
        ChangeDateServiceDialog.showDialog(getSupportFragmentManager(), date, fieldId);
    }

    @Override
    public void changeTime(@NonNull Calendar originTime, int fieldId) {
        // Dialog starten
        Calendar time = originTime == null ? Calendar.getInstance() : originTime;
        ChangeTimeServiceDialog.showDialog(getSupportFragmentManager(), time, fieldId);
    }

    @Override
    public void notifyDateTimeChanged(@NonNull Calendar newDateTime, int fieldId) {
        _viewModel.notifyDateChanged(newDateTime, fieldId);
    }

    private void loadData() {
        // URI für ein Datensatz
        Uri dataUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeDataId);

        Cursor data = getContentResolver().query(dataUri,
                null, // Alle Spalten
                null, //Filter
                null, // Filter Argumente
                null); // Sortierung

        try {
            // Prüfen, ob ein Datensatz da ist
            if (!data.moveToFirst()) {
                return;
            }

            // Startzeit
            int columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME);
            String startDateTimeString = data.getString(columnIndex);
            try {
                _viewModel.setStartTime(TimeDataContract.Converter.parse(startDateTimeString));
            } catch (ParseException e) {
                // Datum konnte nicht umgewandelt werden
                e.printStackTrace();
            }

            // Endzeit
            columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
            if (!data.isNull(columnIndex)) {

                String endDateTimeString = data.getString(columnIndex);
                try {
                    _viewModel.setEndTime(TimeDataContract.Converter.parse(endDateTimeString));
                } catch (ParseException e) {
                    // Datum konnte nicht umgewandelt werden
                    e.printStackTrace();
                }
            }

            // Auslesen der Pause
            columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE);
            int pause = data.getInt(columnIndex);
            _viewModel.setPause(pause);

            // Aslesen des Kommentares
            columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.COMMENT);
            if (!data.isNull(columnIndex)) {
                _viewModel.setComment(data.getString(columnIndex));
            }
        } finally {
            // Ergebnismenge freigeben
            if (data != null) {
                data.close();
            }
        }
    }

    private void saveData() {
        // Spaltenwerte zurodnen
        ContentValues values = new ContentValues();
        values.put(TimeDataContract.TimeData.Columns.START_TIME,
                TimeDataContract.Converter.format(_viewModel.getStartTime()));
        if (_viewModel.getEndTime() == null) {
            values.putNull(TimeDataContract.TimeData.Columns.END_TIME);
        } else {
            values.put(TimeDataContract.TimeData.Columns.END_TIME,
                    TimeDataContract.Converter.format(_viewModel.getEndTime()));
        }
        values.put(TimeDataContract.TimeData.Columns.PAUSE, _viewModel.getPause());
        if (_viewModel.getComment() == null) {
            values.putNull(TimeDataContract.TimeData.Columns.COMMENT);
        } else {
            values.put(TimeDataContract.TimeData.Columns.COMMENT,
                    _viewModel.getComment());
        }

        // Speichern der neuen Daten
        Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeDataId);
        getContentResolver().update(updateUri, values, null, null);
    }
}
