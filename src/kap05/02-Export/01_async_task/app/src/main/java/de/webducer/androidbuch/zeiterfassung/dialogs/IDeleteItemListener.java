package de.webducer.androidbuch.zeiterfassung.dialogs;

public interface IDeleteItemListener {
    void deleteItem(long id, int position);
}
