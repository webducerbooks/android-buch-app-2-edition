package de.webducer.androidbuch.zeiterfassung.services;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.support.annotation.Nullable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class CsvExportService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public CsvExportService() {
        super("CSVExporter");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Cursor data = null;

        try {
            // Daten über Content Provider abfragen
            data = getBaseContext().getContentResolver()
                    .query(TimeDataContract.TimeData.CONTENT_URI,
                            null,
                            null,
                            null,
                            null);

            int dataCount = data == null ? 0 : data.getCount();

            if (dataCount == 0) {
                // Nichts weiter machen, wenn keine Daten vorhanden
                return;
            }

            // Ordner für externe Daten
            File externalStorage = Environment.getExternalStorageDirectory();

            // Prüfen, ob externe Daten geschrieben werden können (SD Karte nur Read Only oder voll)
            if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
                return;
            }

            // Unterordner für unser Export
            File exportPath = new File(externalStorage, "export");

            // Dateiname für Export
            File exportFile = new File(exportPath, "TimeDataLog.csv");

            // Erzeugen der ordner, falls noch nicht vorhanden
            if (!exportFile.exists()) {
                exportPath.mkdirs();
            }

            // Klasse zum Schreiben der Daten
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(exportFile));

                // Asulesen der Spaltennamen
                String[] columnList = data.getColumnNames();

                StringBuilder line = new StringBuilder();

                // Befüllen der ersten Zeile mit Spaltennamen
                for (String columnName : columnList) {
                    if (line.length() > 0) {
                        line.append(';');
                    }

                    line.append(columnName);
                }

                writer.append(line);

                // Zeilen mit Daten ausgeben
                while (data.moveToNext()) {
                    // Neue Zeile
                    writer.newLine();

                    // Zeilenvariable leeren
                    line.delete(0, line.length());

                    // Ausgabe aller Spaltenwerte
                    for (int columnIndex = 0; columnIndex < columnList.length; columnIndex++) {
                        if (line.length() > 0) {
                            line.append(';');
                        }

                        // Prüfen auf NULL (Datenbank) des Spalteninhaltes
                        if (data.isNull(columnIndex)) {
                            line.append("<NULL>");
                        } else {
                            line.append(data.getString(columnIndex));
                        }
                    }

                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    writer.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (writer != null) {
                        writer.flush();

                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (data != null) {
                data.close();
            }
        }
    }
}
