package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class EditDataActivity extends AppCompatActivity {
    public static final String ID_KEY = "TimeDataId";

    private static final long _NO_DATA = -1L;
    private long _timeDataId = _NO_DATA;

    // UI Elemente
    private EditText _startDate;
    private EditText _startTime;
    private EditText _endDate;
    private EditText _endTime;
    private EditText _pause;
    private EditText _comment;

    // Formatter
    DateFormat _dateFormatter = null;
    DateFormat _timeFormatter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_data);

        // Auslesen der übergebenen Metainformationen, flass welche da sind
        _timeDataId = getIntent().getLongExtra(
                ID_KEY, // Key für die Daten
                _NO_DATA // Standardwert, falls nichts übergeben wurde
        );

        // Initialisieren der UI
        _startDate = findViewById(R.id.StartDateValue);
        _startTime = findViewById(R.id.StartTimeValue);
        _endDate = findViewById(R.id.EndDateValue);
        _endTime = findViewById(R.id.EndTimeValue);
        _pause = findViewById(R.id.PauseValue);
        _comment = findViewById(R.id.CommentValue);

        // Initialisieren der Formatter
        _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
        _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Laden der Daten, falls diese vorhanden sind
        if (_timeDataId == _NO_DATA) {
            return;
        }

        loadData();
    }

    private void loadData() {
        // URI für ein Datensatz
        Uri dataUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _timeDataId);

        Cursor data = getContentResolver().query(dataUri,
                null, // Alle Spalten
                null, //Filter
                null, // Filter Argumente
                null); // Sortierung

        try {
            // Prüfen, ob ein Datensatz da ist
            if (!data.moveToFirst()) {
                return;
            }

            // Startzeit
            int columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME);
            String startDateTimeString = data.getString(columnIndex);
            Calendar startDateTime = Calendar.getInstance();
            try{
                startDateTime = TimeDataContract.Converter.parse(startDateTimeString);
            } catch (ParseException e) {
                // Datum konnte nicht umgewandelt werden
                e.printStackTrace();
            }

            // Ausgabe der Startzeit
            _startDate.setText(_dateFormatter.format(startDateTime.getTime()));
            _startTime.setText(_timeFormatter.format(startDateTime.getTime()));

            // Endzeit
            columnIndex = data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
            if(data.isNull(columnIndex)){
                // Keine Werte für Endzeit
                return;
            }

            String endDateTimeString = data.getString(columnIndex);
            Calendar endDateTime = Calendar.getInstance();
            try{
                endDateTime = TimeDataContract.Converter.parse(endDateTimeString);
            } catch (ParseException e) {
                // Datum konnte nicht umgewandelt werden
                e.printStackTrace();
            }

            // Ausgabe der Endzeit
            _endDate.setText(_dateFormatter.format(endDateTime.getTime()));
            _endTime.setText(_timeFormatter.format(endDateTime.getTime()));

            // Auslesen der Pause

            // Aslesen des Kommentares
        } finally {
            // Ergebnismenge freigeben
            if (data != null) {
                data.close();
            }
        }
    }
}
