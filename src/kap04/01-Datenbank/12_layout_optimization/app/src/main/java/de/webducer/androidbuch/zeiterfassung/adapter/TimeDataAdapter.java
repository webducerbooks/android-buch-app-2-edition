package de.webducer.androidbuch.zeiterfassung.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.webducer.androidbuch.zeiterfassung.R;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class TimeDataAdapter extends RecyclerView.Adapter<TimeDataAdapter.TimeDataViewHolder> {
    private final Context _context;
    private Cursor _data = null;
    private DateFormat _dateTimeFormatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

    public TimeDataAdapter(Context context, Cursor data) {
        _context = context;
        _data = data;
    }

    @NonNull
    @Override
    public TimeDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(_context);
        View view = inflater.inflate(R.layout.item_time_data_grid, viewGroup, false);
        return new TimeDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeDataViewHolder timeDataViewHolder, int position) {
        // Keine daten vorhanden
        if (_data == null) {
            return;
        }

        // Keine Daten an der Position
        if (!_data.moveToPosition(position)) {
            return;
        }

        // Daten auslesen
        int columnIndex =
                _data.getColumnIndex(TimeDataContract.TimeData.Columns.START_TIME);
        String startTimeString =
                _data.getString(columnIndex);

        // Konvertieren und formatiren
        try {
            Calendar start = TimeDataContract.Converter.parse(startTimeString);
            startTimeString = _dateTimeFormatter.format(start.getTime());
        } catch (ParseException e) {
            // Fehler bei Konvertierung
            startTimeString = "PARSE ERROR";
        }

        String endTimeString = "---";
        columnIndex =
                _data.getColumnIndex(TimeDataContract.TimeData.Columns.END_TIME);
        if (!_data.isNull(columnIndex)) {
            endTimeString = _data.getString(columnIndex);

            // Konvertieren und formatiren
            try {
                Calendar end = TimeDataContract.Converter.parse(endTimeString);
                endTimeString = _dateTimeFormatter.format(end.getTime());
            } catch (ParseException e) {
                // Fehler bei Konvertierung
                endTimeString = "PARSE ERROR";
            }
        }

        // Daten ins View schreiben
        timeDataViewHolder.StartTime.setText(startTimeString);
        timeDataViewHolder.EndTime.setText(endTimeString);
    }

    @Override
    public int getItemCount() {
        if (_data == null) {
            return 0;
        }

        return _data.getCount();
    }

    public void swapCursor(Cursor newData) {
        if (_data != null) {
            // Cursor schließen
            _data.close();
        }

        _data = newData;
        notifyDataSetChanged();
    }

    class TimeDataViewHolder extends RecyclerView.ViewHolder {
        final TextView StartTime;
        final TextView EndTime;

        public TimeDataViewHolder(@NonNull View itemView) {
            super(itemView);

            // Views suchen
            StartTime = itemView.findViewById(R.id.StartTimeValue);
            EndTime = itemView.findViewById(R.id.EndTimeValue);
        }
    }
}
