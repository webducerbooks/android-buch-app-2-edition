package de.webducer.androidbuch.zeiterfassung.db;

import android.database.sqlite.SQLiteDatabase;

final class TimeDataTable {
    private static final String _CREATE_TABLE =
            "CREATE TABLE \"time_data\" ( `_id` INTEGER PRIMARY KEY AUTOINCREMENT, `start_time` TEXT NOT NULL, `end_time` TEXT )";

    static void createTable(SQLiteDatabase db) {
        db.execSQL(_CREATE_TABLE);
    }
}
