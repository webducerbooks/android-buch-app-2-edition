package de.webducer.androidbuch.zeiterfassung;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import de.webducer.androidbuch.zeiterfassung.adapter.TimeDataAdapter;
import de.webducer.androidbuch.zeiterfassung.db.DbHelper;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class ListDataActivity extends AppCompatActivity {
    private TimeDataAdapter _adapter = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_data);

        // Views suchen
        RecyclerView list = findViewById(R.id.DataList);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setHasFixedSize(true);

        // Adpater initialisieren
        _adapter = new TimeDataAdapter(this, null);
        list.setAdapter(_adapter);

        // Testdaten über Content Provider laden
        Cursor data = getContentResolver()
                .query(TimeDataContract.TimeData.CONTENT_URI,
                        null,null,null,null);
        _adapter.swapCursor(data);
    }
}
