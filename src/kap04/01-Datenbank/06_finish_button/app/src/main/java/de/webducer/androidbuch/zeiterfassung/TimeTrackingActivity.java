package de.webducer.androidbuch.zeiterfassung;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.webducer.androidbuch.zeiterfassung.db.DbHelper;
import de.webducer.androidbuch.zeiterfassung.db.TimeDataContract;

public class TimeTrackingActivity extends AppCompatActivity {
    private EditText _startDateTime;
    private EditText _endDateTime;
    private Button _startCommand;
    private Button _endCommand;

    private final DateFormat _dateTimeFormatter =
            DateFormat.getDateTimeInstance(
                    DateFormat.SHORT, // Datum-Formatierung
                    DateFormat.SHORT); // Uhrzeit-Formatierung

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_tracking);

        // Suchen der Views
        _startDateTime = findViewById(R.id.StartDateTime);
        _endDateTime = findViewById(R.id.EndDateTime);
        _startCommand = findViewById(R.id.StartCommand);
        _endCommand = findViewById(R.id.EndCommand);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Listener registrieren
        _startCommand.setOnClickListener(new StartButtonClicked());

        _endCommand.setOnClickListener(new EndButtonClicked());
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Listener deregistrieren
        _startCommand.setOnClickListener(null);
        _endCommand.setOnClickListener(null);
    }

    class StartButtonClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // Aktuelle Zeit
            Calendar currentTime = Calendar.getInstance();
            // Konvertierung für die Datenbank
            String dbTime = TimeDataContract.Converter.format(currentTime);
            // Zeit für Datenbank
            ContentValues values = new ContentValues();
            values.put(TimeDataContract.TimeData.Columns.START_TIME, dbTime);
            // In die Datenbank speichern
            getContentResolver().insert(TimeDataContract.TimeData.CONTENT_URI, values);
            // Ausgabe für UI
            _startDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));
        }
    }

    class EndButtonClicked implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            // Aktuelle Zeit
            Calendar currentTime = Calendar.getInstance();
            // Konvertierung für die Datenbank
            String dbTime = TimeDataContract.Converter.format(currentTime);
            // Zeit für Datenbank
            ContentValues values = new ContentValues();
            values.put(TimeDataContract.TimeData.Columns.END_TIME, dbTime);
            // In die Datenbank speichern
            getContentResolver().update(TimeDataContract.TimeData.NOT_FINISHED_CONTENT_URI, values, null, null);
            // Ausgabe für UI
            _endDateTime.setText(_dateTimeFormatter.format(currentTime.getTime()));
        }
    }
}
