package de.webducer.androidbuch.zeiterfassung;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class TimeTrackingActivity extends AppCompatActivity {
    private EditText _startDateTime;
    private EditText _endDateTime;
    private Button _startCommand;
    private Button _endCommand;

    private final DateFormat _dateTimeFormatter =
            DateFormat.getDateTimeInstance(
                    DateFormat.SHORT, // Datum-Formatierung
                    DateFormat.SHORT); // Uhrzeit-Formatierung

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_tracking);

        // Suchen der Views
        _startDateTime = findViewById(R.id.StartDateTime);
        _endDateTime = findViewById(R.id.EndDateTime);
        _startCommand = findViewById(R.id.StartCommand);
        _endCommand = findViewById(R.id.EndCommand);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Listener registrieren
        _startCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ausgabe in UI
                Calendar currentTime = Calendar.getInstance();
                _startDateTime.setText(
                        _dateTimeFormatter.format(currentTime.getTime()));
            }
        });

        _endCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar currentTime = Calendar.getInstance();
                _endDateTime.setText(
                        _dateTimeFormatter.format(currentTime.getTime()));
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Listener deregistrieren
        _startCommand.setOnClickListener(null);
        _endCommand.setOnClickListener(null);
    }
}
