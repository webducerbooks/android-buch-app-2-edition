# Android-Apps programmieren

In diesem Projekt finden Sie den Quellcode zu meinem Buch "Android-Apps programmieren" (2. Auflage) bei [**mitp**-Verlag](https://wdurl.de/ab-mitp).

- [Quelcode Herunterladen](https://bitbucket.org/webducerbooks/android-buch-app-2-edition/get/master.zip)

## Korrekturen

### 21.07.2019

- [Korrektur zum Projektanlage-Assistenten](https://wdurl.de/ab-kor-03)
- [Korrektur und Erklärung zu AndroidX](https://wdurl.de/ab-kor-04)

## Changelog

### 09.08.2021

- Aktualisierung für Android Studio 2020.03 (Arctic Fox)
    - Gradle
    - JUnit
    - OkHttp

### 09.07.2021

- Aktualisierung für Android Studio 4.2
  - Gradle Plugin
  - Constraint Layout

### 18.10.2020

- Aktualisierung für Android Studio 4.1
    - Gradle Plugin
    - Constraint Layout
    - JUnit
    - OKHTTP

### 06.03.2020

- Aktualisierung der Abhängigkeiten für Android Studio 3.6.1
	- Gradle Plugin von Version `3.5.1` auf `3.6.1`
	- Gradle von Version `5.4.1` auf `5.6.4`

### 20.10.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.5.1
	- Gradle von Version `5.1.1` auf `5.4.1`
	- OkHttp Bibliothek von Version `4.0.1` auf `4.2.2`
	- Gson Bibliothek von Version `2.8.5` auf `2.8.6`
	- Gradle Plugin von Version `3.4.2` auf `3.5.1`

### 21.07.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.4.2
	- Google Gradle Plugin von Version `3.4.0` auf `3.4.2`
	- OkHttp Bibliothek von Version `3.14.1` auf `4.0.1`

### 22.04.2019

- Aktualisierung der Abhängigkeiten für Android 3.4.0
	- Google Gradle Plugin von Version `3.3.1` auf `3.4.0`
	- OkHttp Bibliothek von Version `3.11.0` auf `3.14.1`
	- Gradle von Version `4.1.0.1` auf `5.1.1`

### 08.02.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.3.1 Kompatibilität
	- Google Gradle Plugion von Version `3.3.0` auf `3.3.1`

### 15.01.2019

- Aktualisierung der Abhängigkeiten für Android Studio 3.3 Kompatibilität
	- Google gradle Plugin von Version `3.2.1` auf `3.3.0`
	- Gradle von Version `4.6` auf `4.10.1`